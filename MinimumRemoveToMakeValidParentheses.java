import java.util.Stack;

class Solution {
    public String minRemoveToMakeValid(String s) {
        Stack<Integer> stack = new Stack<>();
        StringBuilder result = new StringBuilder();

        // First pass: identify and mark unmatched opening parentheses
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (ch == '(') {
                stack.push(i);
            } else if (ch == ')') {
                if (!stack.isEmpty()) {
                    stack.pop();
                } else {
                    // Mark ')' for removal
                    s = s.substring(0, i) + s.substring(i + 1);
                    i--; // adjust index as we removed a character
                }
            }
        }

        // Second pass: remove unmatched opening parentheses
        while (!stack.isEmpty()) {
            int index = stack.pop();
            s = s.substring(0, index) + s.substring(index + 1);
        }

        return s;
    }
}
