//{ Driver Code Starts
// Initial Template for Java

import java.io.*;
import java.util.*;

class GFG {
    public static void main(String args[]) throws IOException {
        BufferedReader read =
            new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(read.readLine());
        while (t-- > 0) {
            String x = read.readLine();
            String y = read.readLine();
            Solution ob = new Solution();
            String result = ob.findSum(x, y);

            System.out.println(result);
        }
    }
}
// } Driver Code Ends


class Solution {
    String findSum(String X, String Y) {
        StringBuilder result = new StringBuilder();
        
        int i = X.length() - 1, j = Y.length() - 1;
        int carry = 0;
        
        // Iterate through both strings from right to left
        while (i >= 0 || j >= 0) {
            int digitX = (i >= 0) ? X.charAt(i) - '0' : 0;
            int digitY = (j >= 0) ? Y.charAt(j) - '0' : 0;
            
            // Add digits along with carry
            int sum = digitX + digitY + carry;
            
            // Calculate new carry
            carry = sum / 10;
            
            // Append least significant digit to result string
            result.append(sum % 10);
            
            i--;
            j--;
        }
        
        // If there's a remaining carry, append it
        if (carry > 0) {
            result.append(carry);
        }
        
        // Reverse the result string
        result.reverse();
        
        // Remove leading zeros
        while (result.length() > 1 && result.charAt(0) == '0') {
            result.deleteCharAt(0);
        }
        
        return result.toString();
    }
}
